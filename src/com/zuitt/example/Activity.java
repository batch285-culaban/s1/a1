package com.zuitt.example;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){

        Scanner myObjects = new Scanner(System.in);

        System.out.println("First Name:");
        String fName = myObjects.nextLine();

        System.out.println("Last Name:");
        String lName = myObjects.nextLine();

        System.out.println("First Subject Grade:");
        Double fSubGrade = myObjects.nextDouble();

        System.out.println("Second Subject Grade:");
        Double sSubGrade = myObjects.nextDouble();

        System.out.println("Third Subject Grade:");
        Double tSubGrade = myObjects.nextDouble();

        Double averageGrade = ((fSubGrade+sSubGrade+tSubGrade)/3);

        int grades = averageGrade.intValue();

        System.out.println("Good day, " + fName +" "+ lName);
        System.out.println("Your grade average is: " + grades);

    }
}
