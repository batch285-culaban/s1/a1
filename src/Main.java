// Main Class
    // entry point of our java program
    // Main Class has 1 method inside, the main method => to run our code
    // It is where running and executing happens

public class Main {
    // static - keyword associated with a method/property that is related in a class. This will allow a method to be invoked without instantiating a class

    // void - keyword that is used to specify a method doesn't return anything. In Java, we have to declare the data type of the method's return
    public static void main(String[] args) {
        // to print statement in the terminal
        System.out.println("Hello world!");
    }
}